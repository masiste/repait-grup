# repair-grup

Si perdiste o se daño el GRUB en Linux, te muestro una forma muy sencilla de repararlo 🙂 
🠟   🠟   🠟 Comandos para copiar y pegar   🠟   🠟   🠟

The first step is run live CD or pendrive with Ubuntu if this is the case and then
execute a terminal and copied the follow commands
```
sudo add-apt-repository ppa:yannubuntu/boot-repair
sudo apt-get update
sudo apt-get install -y boot-repair
boot-repair
```
## Source information

The information is in the youtube channel **tutos PC** and the video in this link

https://www.youtube.com/watch?v=gX7xyd2j3As

